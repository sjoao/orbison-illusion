import os
import math
import time
import numpy as np

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource

# For timing purposes
PROFILE_TIMING = False

# For information about line Dash Patterns, see:
# https://bokeh.pydata.org/en/latest/docs/reference/core/properties.html#bokeh.core.properties.DashPattern
rhombus_dash  = [ 'solid',      'solid',    'solid',    'solid',    'solid',    'solid',    'dotted'  ]
lines_dash    = [ 'solid',      'solid',    'solid',    'solid',    'solid',    'dotted',   'solid'   ]
lines_angles  = [ math.pi/6,    math.pi/6,  math.pi/6,  math.pi/5,  math.pi/7,  math.pi/6,  math.pi/6 ]
lines_spacing = [ 0.025,        0.05,       0.1,        0.05,       0.05,       0.05,       0.05      ]

# Specify the initial parameters of the container, rhombus and lines.
# The width and height values are normalized the canvas,
# and all elements are centered by default.

# Container
container_color = 'Black'
container_line_width = 2
container_width = 0.75
container_height = 0.56

# Rhombus
rhombus_color = 'Red'
rhombus_width = 0.32
rhombus_height = 0.32
rhombus_color = rhombus_color
rhombus_line_width = 4

# Lines
lines_color = 'Blue'

# Container boundaries
container_params = {
    'container_x': [0.5 - container_width/2, 0.5 + container_width/2, 
                    0.5 + container_width/2, 0.5 - container_width/2,
                    0.5 - container_width/2],
    'container_y': [0.5 + container_height/2, 0.5 + container_height/2, 
                    0.5 - container_height/2, 0.5 - container_height/2,
                    0.5 + container_height/2]
}

# Rhombus edges
rhombus_params = {
    'rhombus_x': [0.5 - rhombus_width/2, 0.5,
                  0.5 + rhombus_width/2, 0.5,
                  0.5 - rhombus_width/2],
    'rhombus_y': [0.5, 0.5 + rhombus_height/2,
                  0.5, 0.5 - rhombus_height/2,
                  0.5]
}

staticRsrcFolder = ''

def init(_staticRsrcFolder):
    """This function will be called before the start of the experiment
    and can be used to initialize variables and generate static resources
    
    :param _staticRsrcFolder: path to a folder where static resources can be stored
    """

    global staticRsrcFolder
    staticRsrcFolder = _staticRsrcFolder
    
    # Create figure as global variable and disable axes and tools
    global p
    p = figure(plot_width=500, plot_height=500, x_range=(0, 1), y_range=(0, 1))
    p.toolbar.active_drag = None
    p.toolbar.logo = None
    p.toolbar_location = None
    p.xaxis.visible = None
    p.yaxis.visible = None
    p.xgrid.grid_line_color = None
    p.ygrid.grid_line_color = None

    # Draw container
    global container
    global source_container
    source_container = ColumnDataSource(data=container_params)
    container = p.line('container_x', 'container_y',
                       source=source_container,
                       line_width=container_line_width,
                       color=container_color)
    
    # Draw rhombus
    global rhombus
    global source_rhombus
    source_rhombus = ColumnDataSource(data=rhombus_params)
    rhombus = p.line('rhombus_x', 'rhombus_y',
                     source=source_rhombus,
                     color=rhombus_color,
                     line_width=rhombus_line_width,
                     line_dash=rhombus_dash[0])

    # Draw lines
    global lines
    lines = []
    

def getName():
    """Returns the name of the illusion"""

    return 'Orbison Illusion'

def getInstructions(variationID=None):
    """Returns the instructions as a HTML string"""
    
    instruction = """
        <p>Focus your attention on the <b style=\"color:red\">the red four-sided polygon</b> shown in the image.
            <br/><br/>
        Your task is to adjust the slider so that the polygon is a square.
    """
    return instruction

def getQuestion(variationID=None):
    """Returns a string with a Yes/No question that checks if the participant sees the illusion inverted"""

    return 'Does the rhombus appear to be a square?'

def getNumVariations():
    """Returns the number of variations"""

    return len(rhombus_dash)


def draw(variationID, rhombus_distortion):
    """This function generates the optical illusion figure.
    The function should return a bokeh figure of size 500x500 pixels.

    :param variationID: select which variation to draw (range: 0 to getNumVariations()-1)
    :param distortion: the selected distortion (range: 0.0 to 1.0)
    :return handle to bokeh figure that contains the optical illusion
    """
    
    global p
    global container
    global rhombus
    global lines

    # Clear previous lines
    if lines != []:
        lines.visible = False

    lines = draw_lines(variationID)

    source_rhombus.data.update(
        {
            'rhombus_x': [0.5 - rhombus_width/2 - rhombus_distortion/4 + 1/8, 0.5,
                          0.5 + rhombus_width/2 + rhombus_distortion/4 - 1/8, 0.5,
                          0.5 - rhombus_width/2 - rhombus_distortion/4 + 1/8],
            'rhombus_y': [0.5, 0.5 + rhombus_height/2,
                          0.5, 0.5 - rhombus_height/2,
                          0.5]
        }
    )

    rhombus.glyph.line_dash = rhombus_dash[variationID]

    return p

def draw_lines(variationID):
    """ Computes the vertices of all the background oblique lines,
        then draws these lines in the canvas, using the bokeh multi_line function.

    :param variationID: select which variation to draw (range: 0 to getNumVariations()-1)
    :return array 
    """

    lines = []

    # Get the parameters specific to this variationID
    dash = lines_dash[variationID]
    angle = lines_angles[variationID]
    spacing = lines_spacing[variationID]

    leb, rib, upb, lob = compute_bounds()   # Get the le(ft), ri(ght), up(per) and lo(wer)
                                            #   b(ounds) of the illusion container

    midx = (leb + rib) / 2                  # Compute horizontal coordinate of container center point
    midy = (upb + lob) / 2                  # Compute vertical coordinate of container center point

    x = midx
    dx = (upb - (upb + lob)/2) * math.tan(angle)
    slope = 1/math.tan(angle)

    lefties = np.arange(x, leb, -spacing)
    righties = np.arange(x, rib, spacing)
    outies = np.arange(righties[-1] + spacing, rib + dx, spacing)

    inners = np.unique(np.concatenate([lefties, righties]))

    if PROFILE_TIMING:
        t0 = time.time()

    xs = []
    ys = []
    
    # The following loops compute the positions of all the lines.
    
    # "Inners" are the lines whose inflection point is contained in the image.
    for inner in inners:
        p1 = [inner, midy]
        
        leftx, dy = (leb, (inner - leb) * slope) if leb > inner-dx else (inner-dx, upb-midy)

        p2 = [leftx, midy + dy]
        p3 = [leftx, midy - dy]

        xx, yy = zip(p2, p1, p3)
        xs.append(xx)
        ys.append(yy)

    # "Outies" are the lines which are broken up, i.e., whose inflection point
    #   is to the right of the container.s
    for outie in outies:
        p1 = [outie-dx, upb]
        p2 = [rib, upb - (rib-outie+dx) * slope]

        xx, yy = zip(p1, p2)
        xs.append(xx)
        ys.append(yy)

        p1[1] = lob
        p2[1] = lob + (rib-outie+dx) * slope

        xx, yy = zip(p1, p2)
        xs.append(xx)
        ys.append(yy)
    
    lines = p.multi_line(xs, ys, color=lines_color, line_width=2, line_dash=dash)

    if PROFILE_TIMING:
        t1 = time.time()
        total = t1-t0
        print('Time drawling lines:', total, 's')

    return lines


def compute_bounds():
    """ This function returns the bounds for the container of the illusion
    
    :return (leftBound, rightBound, upperBound, lowerBound) of the container
    """

    return [0.5 - container_width/2,
            0.5 + container_width/2,
            0.5 + container_height/2,
            0.5 - container_height/2]
