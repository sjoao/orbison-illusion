import os
import pandas as pd
import matplotlib.pyplot as plt
import json
from scipy import stats
import numpy

myDirRes = "results/experiment"
myDirQue = 'results/questionnaire'

fList = [f for f in os.listdir(myDirQue) if f.endswith(".json")]
#fListQue = [f for f in os.listdir(myDirQue) if f.endswith(".json")]
subjectDataListRes = []
subjectDataListQue = []

for fName in fList:
	subjectID, _ = os.path.splitext('fName')
	fullFNameRes = myDirRes + "/" + fName
	fullFNameQue = myDirQue + "/" + fName
	subjectDataRes = pd.read_json(fullFNameRes)
	subjectDataQue = json.loads(open(fullFNameQue).read())
	subjectDataListRes.append(subjectDataRes)
	subjectDataListQue.append(subjectDataQue)


dataFrameRes = pd.concat(subjectDataListRes)
dataFrameQue = pd.DataFrame(subjectDataListQue)
dataFrameRes = dataFrameRes.loc[(dataFrameRes['illusionID'] == 8)]
dataFrameRes.to_csv("dataFrameRes.csv")
dataFrameQue.to_csv("dataFrameQue.csv")
#print(dataFrameQue['Gender'].value_counts())
hist = dataFrameQue.hist(column='Age')
plt.savefig("histAge")

var0 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var1 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var2 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var3 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var4 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var5 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var6 = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['distortion']).to_numpy()
var0 = var0.reshape(len(var0))
var1 = var1.reshape(len(var0))
var2 = var2.reshape(len(var0))
var3 = var3.reshape(len(var0))
var4 = var4.reshape(len(var0))
var5 = var5.reshape(len(var0))
var6 = var6.reshape(len(var0))


print(stats.ttest_ind(var1,var0))
print(stats.ttest_ind(var1,var1))
print(stats.ttest_ind(var1,var2))
print(stats.ttest_ind(var1,var3))
print(stats.ttest_ind(var1,var4))
print(stats.ttest_ind(var1,var5))
print(stats.ttest_ind(var1,var6))

#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 0) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar0')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 1) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 1) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar1')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 2) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 2) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar2')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 3) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 3) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar3')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 4) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 4) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar4')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 5) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 5) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar5')
#print(len(dataFrameRes.loc[(dataFrameRes['variationID'] == 6) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion'])))
hist = dataFrameRes.loc[(dataFrameRes['variationID'] == 6) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).hist(column='distortion')
plt.savefig('histVar6')

boxplot = dataFrameRes.loc[((dataFrameRes['variationID'] == 0) | (dataFrameRes['variationID'] == 1) | (dataFrameRes['variationID'] == 2)) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).boxplot(by='variationID')
#boxplot.set_title("Spacing Variations")
boxplot.get_figure().suptitle('')
plt.savefig("boxplotSpacing")

boxplot = dataFrameRes.loc[((dataFrameRes['variationID'] == 3) | (dataFrameRes['variationID'] == 1) | (dataFrameRes['variationID'] == 4)) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).boxplot(by='variationID')
#boxplot.set_title("Angle Variations")
boxplot.get_figure().suptitle('')
plt.savefig("boxplotAngle")

boxplot = dataFrameRes.loc[((dataFrameRes['variationID'] == 1) | (dataFrameRes['variationID'] == 5) | (dataFrameRes['variationID'] == 6)) & (dataFrameRes['inverted'] == 5)].filter(['variationID', 'distortion']).boxplot(by='variationID')
#boxplot.set_title("Line Drawing Variations")
boxplot.get_figure().suptitle('')
plt.savefig("boxplotLining")

